package com.example.covidproject;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.Serializable;
import java.util.ArrayList;

public class ScanActivity extends AppCompatActivity {
    private LinearLayout phoneLinearLayout, headphoneLinearLayout, laptopLinearLayout,otherLinearLayout;
    private TextView textView;
    private Button button;
    private ArrayList<String> addresses = new ArrayList<>();
    private ArrayList<String> phone_safe_list = new ArrayList<>();
    private ArrayList<String> phone_unsafe_list = new ArrayList<>();
    private ArrayList<String> headphone_safe_list = new ArrayList<>();
    private ArrayList<String> headphone_unsafe_list = new ArrayList<>();
    private ArrayList<String> laptop_safe_list = new ArrayList<>();
    private ArrayList<String> laptop_unsafe_list = new ArrayList<>();
    private ArrayList<String> other_safe_list = new ArrayList<>();
    private ArrayList<String> other_unsafe_list = new ArrayList<>();
    private BluetoothAdapter bluetoothAdapter;
    private TextView textViewCounter,tv_phone_safe_counter,tv_phone_unsafe_counter, tv_Headphone_safe_counter,tv_Headphone_unsafe_counter,tv_laptop_safe_counter,
            tv_laptop_unsafe_counter,tv_others_safe_counter,tv_others_unsafe_counter,tv_socialDistance,mMessageView;
    private TextView alert;
    private int counter=0;
    private int phone_safe_counter=0;
    private int phone_unsafe_counter=0;
    private int Headphone_safe_counter=0;
    private int Headphone_unsafe_counter=0;
    private int laptop_safe_counter=0;
    private int laptop_unsafe_counter=0;
    private int others_safe_counter=0;
    private int others_unsafe_counter=0;
    private double socialDistance =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        phoneLinearLayout = findViewById(R.id.clickPhone);
        headphoneLinearLayout = findViewById(R.id.clickHeadphone);
        laptopLinearLayout = findViewById(R.id.clicklaptop);
        otherLinearLayout = findViewById(R.id.clickother);
        textView = (TextView)findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        textViewCounter = (TextView)findViewById(R.id.counter);
        alert = (TextView)findViewById(R.id.alert);
        tv_phone_safe_counter = findViewById(R.id.phone_safe);
        tv_phone_unsafe_counter = findViewById(R.id.phone_unsafe);
        tv_Headphone_safe_counter = findViewById(R.id.headphone_safe);
        tv_Headphone_unsafe_counter = findViewById(R.id.headphone_unsafe);
        tv_laptop_safe_counter = findViewById(R.id.laptop_safe);
        tv_laptop_unsafe_counter = findViewById(R.id.laptop_unsafe);
        tv_others_safe_counter = findViewById(R.id.other_safe);
        tv_others_unsafe_counter = findViewById(R.id.other_unsafe);
        tv_socialDistance = findViewById(R.id.socialDistance);

        //receive intent from DetectionActivity
        //get current intent value

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Bundle");
        //get the data from intent
        //get the value from key named "socialDistance"
        socialDistance = (double)bundle.getSerializable("value");
        Log.d("ScanActivity","fvalue: " + socialDistance);


//        arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,devices);
//        listView.setAdapter(arrayAdapter);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //socialDistance from the intent
        tv_socialDistance.setText("Selected Desirable Distance: " + socialDistance +" meter(s)");
        //set clickable for the each linearlayout false
        phoneLinearLayout.setClickable(false);
        headphoneLinearLayout.setClickable(false);
        laptopLinearLayout.setClickable(false);
        otherLinearLayout.setClickable(false);
        // Register for broadcasts when a device is discovered.
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(bluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(bluetoothAdapter.ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(bluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastReceiver,intentFilter);

    }

    // when the scan button pressed
    public void scanButton(View view){
        textView.setText("  Searching...  ");
        button.setEnabled(false);
        phoneLinearLayout.setClickable(false);
        headphoneLinearLayout.setClickable(false);
        laptopLinearLayout.setClickable(false);
        otherLinearLayout.setClickable(false);
        //clear the arraylist
        addresses.clear();
        phone_safe_list.clear();
        phone_unsafe_list.clear();
        headphone_safe_list.clear();
        headphone_unsafe_list.clear();
        laptop_safe_list.clear();
        laptop_unsafe_list.clear();
        other_safe_list.clear();
        other_unsafe_list.clear();
        //initialize all variables
        counter=0;
        phone_safe_counter=0;
        phone_unsafe_counter=0;
        Headphone_safe_counter=0;
        Headphone_unsafe_counter=0;
        laptop_safe_counter=0;
        laptop_unsafe_counter=0;
        others_safe_counter=0;
        others_unsafe_counter=0;
        tv_others_unsafe_counter.setText("0");
        tv_others_safe_counter.setText("0");
        tv_laptop_unsafe_counter.setText("0");
        tv_laptop_safe_counter.setText("0");
        tv_phone_unsafe_counter.setText("0");
        tv_phone_safe_counter.setText("0");
        tv_Headphone_unsafe_counter.setText("0");
        tv_Headphone_safe_counter.setText("0");
        textViewCounter.setText("");
        alert.setText("");

        //check if bluetooth adapter is enable before the scanning
        if(!bluetoothAdapter.isEnabled()){
            Toast.makeText(getApplicationContext(),"open bluetooth to scan devices",Toast.LENGTH_SHORT).show();
        }else{
            //start discovery when the bluetooth adapter is enable
            bluetoothAdapter.startDiscovery();
        }

    }

    //create a broadcast receiver for the ACTION FOUND
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            //if the discovery finished, then set text as finished
            if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                textView.setText("  Finished!");
                //set each layout clickable true when searching finished
                phoneLinearLayout.setClickable(true);
                headphoneLinearLayout.setClickable(true);
                laptopLinearLayout.setClickable(true);
                otherLinearLayout.setClickable(true);
                //send alarm if each counter >= 1 after finishing scanning part
                if(others_unsafe_counter >= 1 || laptop_unsafe_counter >= 1 || phone_unsafe_counter >= 1 || Headphone_unsafe_counter >= 1){
                    showAlertDialog();
                }else{
                    alert.setText("You are in a safe social distance\nKeep It Up !!");
                }
                button.setEnabled(true);
            }
            else if(BluetoothDevice.ACTION_FOUND.equals(action)){
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String name = device.getName();
                String address = device.getAddress();
                //initialize rssi value
                short rssi = intent.getExtras().getShort(device.EXTRA_RSSI);
                Log.d("ScanActivity", "onReceive found device, device address = " + address + ",deviceName = " + name + "  distance: " + String.valueOf(getDistance(rssi)) + "meter(s)");
                Log.d("ScanActivity","the device class is: " + device.getBluetoothClass().getMajorDeviceClass());
                Log.d("ScanActivity","the selected Social distance is: " + socialDistance);
                if(!addresses.contains(address)){
                    addresses.add(address);

                    //format device info string
                    String deviceString ="";

                    if(name == null || name.equals("")){

                        deviceString = "Device name/address: " + address + "\nRSSI: " + rssi + "dBm\nDistance: " + getDistance(rssi) +"meter(s)";
                    }
                    else{
                        deviceString = "Device name/address: " + name + "\nRSSI: " + rssi + "dBm\nDistance: " + getDistance(rssi) +"meter(s)";
                    }

                    if(isPhoneDevice(device)){
                        if(isSafe(rssi, socialDistance)){
                            //if it's a safe distance, make the counter plus
                            phone_safe_counter++;
                            tv_phone_safe_counter.setText(phone_safe_counter+"");
                            //store in arraylist
                            phone_safe_list.add(deviceString);
                        }
                        else {
                            phone_unsafe_counter++;
                            counter++;
                            tv_phone_unsafe_counter.setText(phone_unsafe_counter+"");
                            //store in unsafephone list
                            phone_unsafe_list.add(deviceString);
                        }
                    }
                    else if(isHeadphoneDevice(device)){
                        if(isSafe(rssi,socialDistance)){
                            Headphone_safe_counter++;
                            tv_Headphone_safe_counter.setText(Headphone_safe_counter+"");
                            //store in safe headphone list
                            headphone_safe_list.add(deviceString);
                        }
                        else{
                            Headphone_unsafe_counter++;
                            counter++;
                            tv_Headphone_unsafe_counter.setText(Headphone_unsafe_counter+"");
                            //store in unsafe headphone list
                            headphone_unsafe_list.add(deviceString);
                        }

                    }
                    else if(isLaptopDevice(device)){
                        if(isSafe(rssi,socialDistance)){
                            laptop_safe_counter++;
                            tv_laptop_safe_counter.setText(laptop_safe_counter+"");
                            //store in safe laptop list
                            laptop_safe_list.add(deviceString);
                        }
                        else{
                            laptop_unsafe_counter++;
                            counter++;
                            tv_laptop_unsafe_counter.setText(laptop_unsafe_counter+"");
                            //store in unsafe laptop list
                            laptop_unsafe_list.add(deviceString);
                        }

                    }
                    else{
                        if(isSafe(rssi,socialDistance)){
                            others_safe_counter++;
                            tv_others_safe_counter.setText(others_safe_counter+"");
                            //store in safe other list
                            other_safe_list.add(deviceString);
                        }
                        else{
                            others_unsafe_counter++;
                            counter++;
                            tv_others_unsafe_counter.setText(others_unsafe_counter+"");
                            //store in unsafe other list
                            other_unsafe_list.add(deviceString);
                        }
                    }
                }


            }
        }
    };

    //check if it's safe by compare the distance calculated from rssi between desirable distance
    private boolean isSafe(double Rssi, double socialDistance){
        //get the distance from rssi
        double distance = getDistance(Rssi);
        Log.d("ScanActivity","the distance between primary device and bluetooth device is: " + String.valueOf(distance));
        //compare
        if(socialDistance < distance){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean isPhoneDevice(BluetoothDevice bluetoothDevice){
        // HEX: PHONE = 0x0200   DEC = 512 ;
        int device = bluetoothDevice.getBluetoothClass().getMajorDeviceClass();
        if(device == 512){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean isLaptopDevice(BluetoothDevice bluetoothDevice){
        // HEX: COMPUTER = 0x0100   DEC = 256 ;
        int device = bluetoothDevice.getBluetoothClass().getMajorDeviceClass();
        if(device == 256){
            return true;
        }
        else{
            return false;
        }
    }

    private boolean isHeadphoneDevice(BluetoothDevice bluetoothDevice){
        // HEX: AUDIO_VIDEO = 0x0400   DEC = 1024 ;
        int device = bluetoothDevice.getBluetoothClass().getMajorDeviceClass();
        if(device == 1024){
            return true;
        }
        else{
            return false;
        }
    }

    //calculate the distance between two devices
    private double getDistance(double Rssi) {
        double rssi = Math.abs(Rssi);
        rssi = (rssi - 60) / 33.0;
        double distance= Math.pow(10.0, rssi);
        distance = (double) Math.round(distance * 100) / 100;
        return distance;
    }

    //vibrate start
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void starVibrate(final Activity activity, long[] pattern, boolean isRepeat) {
        Vibrator vib = (Vibrator) activity.getSystemService(Service.VIBRATOR_SERVICE);
        vib.vibrate(pattern, isRepeat ? 1 : -1);
    }
    //vibrate end
    public void stopVibrate(final Activity activity) {
        Vibrator vib = (Vibrator) activity.getSystemService(Service.VIBRATOR_SERVICE);
        vib.cancel();
    }

    // show Alarm dialog
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showAlertDialog(){
        //start vibrate
        starVibrate(ScanActivity.this, new long[]{100, 200, 100, 200},true);
        //initialize dialog
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Alarm")
                .setMessage("There are " + counter + " unsafe device/s around." +"\nYou are in an unsafe social distance !!\n Please leave as soon as possible !!!")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //stop the vibrate
                        stopVibrate(ScanActivity.this);
                    }
                })
                .create();
        dialog.show();
        //set clickable false when click outside
        dialog.setCanceledOnTouchOutside(false);
        //set button color
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#006400"));
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setAllCaps(false);
        //set message size and color and gravity
        mMessageView = dialog.findViewById(android.R.id.message);
        mMessageView.setTextSize(14);
        mMessageView.setGravity(Gravity.CENTER);
        mMessageView.setTextColor(Color.parseColor("#DC143C"));

    }

    //set clickphone event
    public void ClickPhone(View view){
        //  Log.d("ScanActivity","clickPhone isclickable: "+ phoneLinearLayout.isClickable());
        if(phoneLinearLayout.isClickable()){
            //jump to clickPhone activity
            Intent intent = new Intent(this,ClickPhoneActivity.class);
            Bundle bundle = new Bundle();
            // bundle.putStringArrayList("devices",(ArrayList<String>) devices);
            bundle.putSerializable("phoneSafe",(Serializable)phone_safe_list);
            bundle.putSerializable("phoneUnsafe",(Serializable)phone_unsafe_list);
            intent.putExtra("Bundle",bundle);
            startActivity(intent);
        }
    }

    //set click headphone event
    public void ClickHeadphone(View view){
        //  Log.d("ScanActivity","clickHeadphone isclickable: "+ headphoneLinearLayout.isClickable());
        if(headphoneLinearLayout.isClickable()){
            //jump to clickPhone activity
            Intent intent = new Intent(this,ClickHeadphoneActivity.class);
            Bundle bundle = new Bundle();
            // bundle.putStringArrayList("devices",(ArrayList<String>) devices);
            bundle.putSerializable("HeadphoneSafe",(Serializable)headphone_safe_list);
            bundle.putSerializable("HeadphoneUnsafe",(Serializable)headphone_unsafe_list);
            intent.putExtra("Bundle",bundle);
            startActivity(intent);
        }
    }

    //set click laptop event
    public void ClickLaptop(View view){
        //Log.d("ScanActivity","clicklaptop isclickable: "+ laptopLinearLayout.isClickable());
        if(laptopLinearLayout.isClickable()){
            //jump to clickPhone activity
            Intent intent = new Intent(this,ClickLaptopActivity.class);
            Bundle bundle = new Bundle();
            // bundle.putStringArrayList("devices",(ArrayList<String>) devices);
            bundle.putSerializable("LaptopSafe",(Serializable)laptop_safe_list);
            bundle.putSerializable("LaptopUnsafe",(Serializable)laptop_unsafe_list);
            intent.putExtra("Bundle",bundle);
            startActivity(intent);
        }
    }

    //set click other event
    public void ClickOther(View view){
        //Log.d("ScanActivity","clickother isclickable: "+ otherLinearLayout.isClickable());
        if(otherLinearLayout.isClickable()){
            //jump to clickPhone activity
            Intent intent = new Intent(this,ClickOtherActivity.class);
            Bundle bundle = new Bundle();
            // bundle.putStringArrayList("devices",(ArrayList<String>) devices);
            bundle.putSerializable("OtherSafe",(Serializable)other_safe_list);
            bundle.putSerializable("OtherUnsafe",(Serializable)other_unsafe_list);
            intent.putExtra("Bundle",bundle);
            startActivity(intent);
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // unregister the ACTION_FOUND receiver.
        unregisterReceiver(broadcastReceiver);
    }
}