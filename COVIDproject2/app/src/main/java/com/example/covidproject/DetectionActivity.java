package com.example.covidproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;

import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.Set;

public class DetectionActivity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 1;

    //initialize variable
    private TextView mStatusBlueTv, mPairedTv;
    private ImageView mBlueIv;
    private Button mOnBtn, mOffBtn, mDiscoverBtn, mPairedBtn;
    private double fvalue =0;
    BluetoothAdapter mBlueAdapter;
    DrawerLayout drawerLayout;
    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detection);

        //assign variable
        drawerLayout = findViewById(R.id.drawer_layout);
        mStatusBlueTv = findViewById(R.id.bt_status);
        mPairedTv     = findViewById(R.id.tv_paired);
        mBlueIv       = findViewById(R.id.bt_ic);
        mOnBtn        = findViewById(R.id.bt_on);
        mOffBtn       = findViewById(R.id.bt_off);
        mDiscoverBtn  = findViewById(R.id.bt_discover);
        mPairedBtn    = findViewById(R.id.bt_paired);
        //set listeners for the button click
        setListeners();
        //adapter
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();

        //check if bluetooth is available or not
        if (mBlueAdapter == null){
            mStatusBlueTv.setText("Bluetooth is not available");
        }
        else {
            mStatusBlueTv.setText("Bluetooth is available");
        }

        //set image according to bluetooth status(on/off)
        if (mBlueAdapter.isEnabled()){
            mBlueIv.setImageResource(R.drawable.ic_action_bluetooth_on);
        }
        else {
            mBlueIv.setImageResource(R.drawable.ic_action_bluetooth_off);
        }
        //get current intent value
        String value = "1.5";
        Intent intent = this.getIntent();
        //get the data from intent
        Bundle bundle = intent.getExtras();
        //if detectionActivity recreate itself, then set the default value as 1.5 meter
        if(bundle != null && bundle.containsKey("value")){
            //get the value from key named "value"
            value = bundle.getString("value");
            bundle.clear();
        }
        try{
            fvalue = Double.parseDouble(value);
            Log.d("DetectionActivity","fvalue: " + fvalue);
        }catch (NumberFormatException e){
        }
       // Log.d("DetectionActivity",value);

    }
    //set listeners
    private void setListeners(){
        DetectionActivity.OnClick onClick = new DetectionActivity.OnClick();
        mOffBtn.setOnClickListener(onClick);
        mOnBtn.setOnClickListener(onClick);
        mDiscoverBtn.setOnClickListener(onClick);
        mPairedBtn.setOnClickListener(onClick);
    }
    // created Onclick method implements onClickListener
    private class OnClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Intent intent = null;
            switch (v.getId()){
                case R.id.bt_off:
                    if (mBlueAdapter.isEnabled()){
                        mBlueAdapter.disable();
                        showToast("Turning Bluetooth Off");
                        mBlueIv.setImageResource(R.drawable.ic_action_bluetooth_off);
                    }
                    else {
                        showToast("Bluetooth is already off");
                    }
                    break;
                case R.id.bt_on:
                    if (!mBlueAdapter.isEnabled()){
                        showToast("Turning On Bluetooth...");
                        //intent to on bluetooth
                        intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(intent, REQUEST_ENABLE_BT);
                    }
                    else {
                        showToast("Bluetooth is already on");
                    }
                    break;
                case R.id.bt_discover:
                    if (!mBlueAdapter.isDiscovering()){
                        showToast("Making Your Device Discoverable");
                        intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                        startActivityForResult(intent, REQUEST_DISCOVER_BT);
                    }
                    break;
                case R.id.bt_paired:
                    if (mBlueAdapter.isEnabled()){
                        mPairedTv.setText("Paired Devices");
                        Set<BluetoothDevice> devices = mBlueAdapter.getBondedDevices();

                        for (BluetoothDevice device: devices){
                            mPairedTv.append("\nDevice: " + device.getName()+ ", " + device.getAddress());
                        }

                    }
                    else {
                        //bluetooth is off so can't get paired devices
                        showToast("Turn on bluetooth to get paired devices");
                    }
                    break;
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK){
                    //bluetooth is on
                    mBlueIv.setImageResource(R.drawable.ic_action_bluetooth_on);
                    showToast("Bluetooth is on");
                }
                else {
                    //user denied to turn bluetooth on
                    showToast("could't on bluetooth");
                }
                break;
            case REQUEST_DISCOVER_BT:
                if(requestCode != RESULT_OK){
                    //bluetooth is discovering
                    mBlueIv.setImageResource(R.drawable.ic_action_bluetooth_discover);
                }
              //  Log.d("DetectionActivity",  "requestcode: "+requestCode);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    //toast message function
    private void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public void ClickMenu(View view){
        //open drawer
        MainActivity.openDrawer(drawerLayout);
    }
    public void ClickLogo(View view){
        //close drawer
        MainActivity.closeDrawer(drawerLayout);
    }
    public void ClickHome(View view){
        //recreate
        MainActivity.redirectActivity(this,MainActivity.class);
    }

    public void ClickSetting(View view){
        //redirect activity to Setting
        MainActivity.redirectActivity(this,SettingActivity.class);

    }
    public void ClickDetection(View view){
        //recreate activity
        MainActivity.redirectActivity(this,DetectionActivity.class);
      //  recreate();
    }
    public void ClickQuit(View view){
        //close app
        MainActivity.quit(this);
    }
    public void ClickScan(View view){
        Intent intent = new Intent(this,ScanActivity.class);
        //send value of desirable value to ScanActivity
        Bundle bundle = new Bundle();
        // bundle.putStringArrayList("devices",(ArrayList<String>) devices);
        bundle.putSerializable("value",(Serializable)fvalue);
        intent.putExtra("Bundle",bundle);
        startActivity(intent);
    }
    public void onPause() {

        super.onPause();
        //close drawer
        MainActivity.closeDrawer(drawerLayout);
    }
}