package com.example.covidproject;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ClickHeadphoneActivity extends AppCompatActivity {
    private ListView lv_safe,lv_unsafe;
    private TextView tv_safe,tv_unsafe;
    private ArrayList<String> HeadphoneSafe = new ArrayList<String>();
    private ArrayList<String> HeadphoneUnsafe = new ArrayList<String>();
    private ArrayAdapter aa_safe,aa_unsafe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_click_head_phone);

        lv_safe = findViewById(R.id.lv_headphone_safe);
        lv_unsafe = findViewById(R.id.lv_headphone_unsafe);
        tv_safe = findViewById(R.id.tv_safe_nothingfound);
        tv_unsafe = findViewById(R.id.tv_unsafe_nothingfound);
        //receive bundle from ScanActivity
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("Bundle");
        //get data from bundle
        HeadphoneSafe = (ArrayList<String>)bundle.getSerializable("HeadphoneSafe");
        HeadphoneUnsafe = (ArrayList<String>)bundle.getSerializable("HeadphoneUnsafe");
        if(!HeadphoneSafe.isEmpty()){
            //store in listview
            aa_safe = new ArrayAdapter(this, android.R.layout.simple_list_item_1,HeadphoneSafe);
            //set adapter
            lv_safe.setAdapter(aa_safe);
        }
        else{
            //set the content text as nothing
            tv_safe.setText("Nothing Found");
        }
        if(!HeadphoneUnsafe.isEmpty()){
            //store in listview
            aa_unsafe = new ArrayAdapter(this, android.R.layout.simple_list_item_1,HeadphoneUnsafe);
            //set adapter
            lv_unsafe.setAdapter(aa_unsafe);
        }
        else{
            //set the content text as nothing
            tv_unsafe.setText("Nothing Found");
        }
        Log.d("ClickHeadPhoneActivity", "safelist: "+ HeadphoneSafe + "unsafelist: " + HeadphoneUnsafe);
    }
}