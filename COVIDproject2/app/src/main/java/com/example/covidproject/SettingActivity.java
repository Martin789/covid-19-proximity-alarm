package com.example.covidproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

public class SettingActivity extends AppCompatActivity {
    //set a gloabal variable to store distance value
    String current_value = "1.5";
    //initialize variable
    DrawerLayout drawerLayout;
    private TextView mTvNP;
    private NumberPicker numberPicker;
    private Button mBtnNext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        //assign variable
        drawerLayout = findViewById(R.id.drawer_layout);
        mTvNP = findViewById(R.id.tv_numberpicker);
        numberPicker = findViewById(R.id.numberpicker);
        mBtnNext = findViewById(R.id.bt_toMain);


        //set value of number picker
        String[] socialDistance = {
                "1.5","1.6","1.7","1.8","1.9","2.0",
                "2.1","2.2","2.3","2.4","2.5",
                "2.6","2.7","2.8","2.9","3.0" };
        numberPicker.setDisplayedValues(socialDistance);
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(socialDistance.length-1);
        numberPicker.setValue(0);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mTvNP.setText("Selected: "+socialDistance[newVal] +" meter(s)");
                current_value = socialDistance[newVal];
            }
        });
//        numberPicker.setMaxValue(30);
//        numberPicker.setMinValue(15);
//        numberPicker.setFormatter(new NumberPicker.Formatter() {
//            @Override
//            public String format(int value) {
//                return String.valueOf((float) value/10);
//            }
//        });
//        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
//                mTvNP.setText("Selected: "+ (float)newVal/10 + "  meter");
//            }
//        });
        //set onclick listener for next button
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this,DetectionActivity.class);
                //send value of new distance value to DetectionActivity
                intent.putExtra("value",current_value);
                startActivity(intent);
            }
        });

    }
    public void ClickMenu(View view){
        //open drawer
        MainActivity.openDrawer(drawerLayout);
    }
    public void ClickLogo(View view){
        //close drawer
        MainActivity.closeDrawer(drawerLayout);
    }
    public void ClickHome(View view){
        //recreate
        MainActivity.redirectActivity(this,MainActivity.class);
    }

    public void ClickSetting(View view){
        //recreate activity
        recreate();
    }
    public void ClickDetection(View view){
        //redirect activity to detection
        MainActivity.redirectActivity(this,DetectionActivity.class);

    }
    public void ClickQuit(View view){
        //close app
        MainActivity.quit(this);
    }
    public void onPause() {

        super.onPause();
        //close drawer
        MainActivity.closeDrawer(drawerLayout);
    }



}