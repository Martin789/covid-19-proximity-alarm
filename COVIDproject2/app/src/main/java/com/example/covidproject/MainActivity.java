package com.example.covidproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
//initialize variable
    DrawerLayout drawerLayout;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //assign variable
        drawerLayout = findViewById(R.id.drawer_layout);
        button = findViewById(R.id.bt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SettingActivity.class);
                startActivity(intent);
            }
        });
    }

    public void ClickMenu(View view){
        openDrawer(drawerLayout);
    }



    public void ClickLogo(View view){
        //close drawer
        closeDrawer(drawerLayout);
    }


    public void ClickHome(View view){
        //recreate activity
        recreate();
    }

    public void ClickSetting(View view){
        //redirect activity to setting
        redirectActivity(this,SettingActivity.class);
    }

    public void ClickDetection(View view){
        //redirect activity to detection
        redirectActivity(this,DetectionActivity.class);
    }

    public static void quit(Activity activity){
        //initialize alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        //set title
        builder.setTitle("Quit");
        //set message
        builder.setMessage("Are you sure you want to Quit ?");
        //positive yes button
        builder.setPositiveButton("YES",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish activity
                activity.finishAffinity();
                //exit app
                System.exit(0);
            }
        });
        //negative no button
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //dismiss dialog
                dialog.dismiss();
            }
        });
        //show dialog
        builder.show();
    }

    public static void redirectActivity(Activity activity,Class aclass) {
        //initialize intent
        Intent intent = new Intent(activity,aclass);
        //set flags
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //start activity
        activity.startActivity(intent);

    }
    //click quit event
    public void ClickQuit(View view){
        //close app
        MainActivity.quit(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        //close drawer
        closeDrawer(drawerLayout);
    }

    public static void openDrawer(DrawerLayout drawerLayout){
        //open drawer layout
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public static void closeDrawer(DrawerLayout drawerLayout){
        //close drawer layout
        //check the condition
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            //when drawer is open, then close the drawer
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}