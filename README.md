# Covid-19 Proximity Alarm
**In order to build and run the project successfully, the following conditions are preferred:**

- Windows 10 with Git version: 2.30.0.windows.2.
- Java OpenJDK version: 15 (set environment variable).
- IDE: Android Studio (version: 4.1.1).
- Android SDK platform: Android 11 with API level: 30.
- Physical Android Device.

**After everything is set up, first clone the remote repositoryfrom the provided URL to the local machine via Git. 
Then open the project with AndroidStudio. Before running the code, the debugging device needs to be set up. Note that the debugging device must be a physical Android device, not anemulator, because thedebugger does not support Bluetooth. Finally, click the "Run" button in the upper right corner of AndroidStudio to run the project
android application for social distancing **
